<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth/')->group(function() {
    Route::post('/sign-up', [\App\Http\Controllers\AuthController::class, 'createAction']);
    Route::post('/sign-in', [\App\Http\Controllers\AuthController::class, 'loginAction']);
    Route::get('/logout', [\App\Http\Controllers\AuthController::class, 'logout']);
});

cwd = $(shell pwd)
dc_app_name=fizzy

CURRENT_USER = $(shell id -u):$(shell id -g)
SHELL = /bin/sh
DC_BIN = $(shell command -v docker-compose 2> /dev/null)
DOCKER_BIN = $(shell command -v docker 2> /dev/null)
RUN_APP_ARGS = --rm --user "$(CURRENT_USER)" "$(dc_app_name)"
BACKEND_PORT := 80

shell:
	$(DC_BIN) run -e STARTUP_WAIT_FOR_SERVICES=false $(RUN_APP_ARGS) bash

pre-init: ## Make full application pre-initialization (install, seed, build assets, etc)
	$(DC_BIN) run -e "STARTUP_DELAY=5" "$(dc_app_name)" ./artisan migrate --force --no-interaction
	$(DC_BIN) run -e "STARTUP_DELAY=5" "$(dc_app_name)" ./artisan db:seed --class=RoleAndPermissionSeeder

clean: ## Make cleaning
	$(DC_BIN) down -v -t 1 # volumes removal is important
	$(DC_BIN) run --no-deps "$(dc_app_name)" composer clear

composer-install: ## Install all app dependencies
	$(DOCKER_BIN) run --rm \
        -u "$(id -u):$(id -g)" \
        -v $(cwd):/var/www/html \
        -w /var/www/html \
        laravelsail/php81-composer:latest \
        composer install --ignore-platform-reqs

update: ## Update all app dependencies
	$(DC_BIN) run -e STARTUP_WAIT_FOR_SERVICES=false $(RUN_APP_ARGS) composer update -n --ansi --prefer-dist

init: composer-install pre-init

up:
	CURRENT_USER="$(CURRENT_USER)" $(DC_BIN) up --detach fizzy
	$(call print_block, 'Navigate to ⇒ https://0.0.0.0:$(BACKEND_PORT)')

down: ## Stop and remove containers, networks, images, and volumes
	$(DC_BIN) down -t 5

restart: down up

logs: ## Show docker logs
	$(DC_BIN) logs --follow

<?php

namespace Database\Seeders;

use App\DTO\Role\Role;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Role::ROLES_MAP as $role => $permissions) {
            /** @var \Spatie\Permission\Models\Role $role */
            $role = \Spatie\Permission\Models\Role::create(['name' => $role]);

            if (\is_array($permissions)) {
                foreach ($permissions as $permission) {
                    Permission::create(['name' => $permission]);
                }
            }

            $role->syncPermissions($permissions);
        }
    }
}

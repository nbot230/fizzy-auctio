FROM ghcr.io/roadrunner-server/roadrunner:2.10.2 AS roadrunner

FROM php:8.1.6-cli as base

ARG DEBIAN_FRONTEND=noninteractive
ARG GID=1000
ARG UID=1000

RUN apt update && \

    # Install tools
    apt install -y --no-install-recommends \
        libxml2-utils \
        unzip \
        wget \
        git && \

    apt install -y --no-install-recommends \
    libpq-dev libicu-dev libxslt-dev && \
    docker-php-ext-configure intl && \
    docker-php-ext-install pdo pdo_pgsql pgsql bcmath intl xsl sysvsem && \

  # Install zip php module
    apt install -y --no-install-recommends libzip-dev && \
    docker-php-ext-install zip && \

    # Install sockets php module
    docker-php-ext-install sockets && \

    # Install imagick php module
    apt install -y --no-install-recommends libmagick++-dev && \
    pecl install imagick && \
    docker-php-ext-enable imagick && \

    # Install amqp
    apt install -y --no-install-recommends librabbitmq-dev && \
    pecl install https://github.com/0x450x6c/php-amqp/raw/7323b3c9cc2bcb8343de9bb3c2f31f6efbc8894b/amqp-1.10.3.tgz && \
    docker-php-ext-enable amqp && \

    # Install redis
    pecl install redis && \
    docker-php-ext-enable redis

# Install gosu
RUN wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/1.13/gosu-$(dpkg --print-architecture)" && \
    chmod +x /usr/local/bin/gosu

# Install composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN groupadd --gid ${GID} user && useradd --uid ${UID} -s /bin/bash -m -g user user

COPY --from=roadrunner /usr/bin/rr /usr/local/bin/rr

RUN pecl install xdebug-3.1.5 && docker-php-ext-enable xdebug
RUN chmod 777 /opt

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN sed -ri "s#memory_limit.*#memory_limit = 1G#" "$PHP_INI_DIR/php.ini"

COPY ./etc /

WORKDIR /app

<?php

namespace App\Traits\User;

use Illuminate\Support\Str;

trait HasEmailAttributeTrait
{
    /**
     * Set the normalized email.
     *
     * @param ?string $value
     */
    protected function setEmailAttribute($value): void
    {
        $this->attributes['email'] = static::normalizeEmail($value);
    }
}

<?php

namespace App\Event;

use App\DTO\User\UserDTO;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ClientRegisteredEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public UserDTO $userData;

    /**
     * @param UserDTO $userDTO
     */
    public function __construct(UserDTO $userDTO)
    {
           $this->userData = $userDTO;
    }
}

<?php

namespace App\Service;

use Illuminate\Support\Facades\Redis;

class RedisService
{
    /**
     * @param string $key
     * @param string $value
     */
    public static function set(string $key, string $value): void
    {
        Redis::set($key, $value);
    }

    /**
     * @param string $key
     *
     * @return string|null
     */
    public static function get(string $key): ?string
    {
        return Redis::get($key) ?? null;
    }
}

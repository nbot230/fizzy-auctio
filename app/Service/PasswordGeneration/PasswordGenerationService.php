<?php

namespace App\Service\PasswordGeneration;

use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Hackzilla\PasswordGenerator\Generator\HumanPasswordGenerator;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class PasswordGenerationService implements PasswordGenerationServiceInterface
{
    /**
     * Путь к словарю для генерации человеческих паролей.
     *
     * @var string
     */
    protected static $dictionary_path = __DIR__ . '/../dictionaries/english.txt';

    /**
     * Символы для большей рандомизации пароля.
     *
     * @var array<string>
     */
    protected static $symbols = ['!', '@', '#', '$', '%', '&', '*', '+', '?'];

    /**
     * Генерирует человеко-понятный пароль.
     *
     * @param int    $words_count
     * @param string $words_separator
     * @param int    $min_length
     * @param int    $max_length
     *
     * @return string
     */
    public static function humanPassword(int $words_count = 2,
                                         string $words_separator = '-',
                                         int $min_length = 6,
                                         int $max_length = 64): string
    {
        $generator = static::getHumanPasswordGenerator();

        $generator->setWordCount($words_count)
            ->setWordSeparator($words_separator)
            ->setMinWordLength((int) ($min_length / $words_count))
            ->setMaxWordLength((int) ($max_length / $words_count));

        $passwords_list = static::getHumanPasswordGenerator()->generatePasswords(1);

        $password = (\random_int(0, 1) < 0.5)
            ? Str::ucfirst($passwords_list[0])
            : $passwords_list[0];

        return  $password . Arr::random(static::$symbols) . \random_int(10, 99);
    }

    /**
     * Генерирует случайную строку.
     *
     * @param int $length
     *
     * @return string
     */
    public static function computerPassword(int $length = 6): string
    {
        $generator = new ComputerPasswordGenerator;

        $generator
            ->setUppercase()
            ->setLowercase()
            ->setNumbers()
            ->setSymbols(false)
            ->setLength($length);

        $password = $generator->generatePasswords(1);

        return $password[0];
    }

    /**
     * Загружает объект со словарём для генерации человеко-понятных паролей.
     *
     * @return HumanPasswordGenerator
     */
    protected static function getHumanPasswordGenerator(): HumanPasswordGenerator
    {
        /** @var HumanPasswordGenerator|null */
        static $generator = null;

        if (! ($generator instanceof HumanPasswordGenerator)) {
            $generator = new HumanPasswordGenerator;
            $generator->setWordList(static::$dictionary_path);
        }

        return $generator;
    }
}

<?php

namespace App\Service\PasswordGeneration;

interface PasswordGenerationServiceInterface
{
    /**
     * Генерирует человеко-понятный пароль.
     *
     * @param int    $words_count
     * @param string $words_separator
     * @param int    $min_length
     * @param int    $max_length
     *
     * @return string
     */
    public static function humanPassword(int $words_count, string $words_separator, int $min_length, int $max_length): string;

    /**
     * Генерирует случайную строку.
     *
     * @param int $length
     *
     * @return string
     */
    public static function computerPassword(int $length): string;
}

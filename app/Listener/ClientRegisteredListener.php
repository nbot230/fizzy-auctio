<?php

namespace App\Listener;

use App\Event\ClientRegisteredEvent;
use App\Helper\Log\FizzyLoggerHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class ClientRegisteredListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var
     */
    private $tries;

    /**
     * @var Client $client
     */
    private $client;

    /**
     * @var string
     */
    private $uri;

    public function __construct(Client $client, int $tries, string $uri)
    {
        $this->client = $client;
        $this->tries = $tries;
        $this->uri = $uri;
    }

    /**
     * @param ClientRegisteredEvent $event
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function handle(ClientRegisteredEvent $event)
    {
        Log::info(FizzyLoggerHelper::getListenerLogPrefix() . 'Начинаем отправку EMAIL для верификации пользователя');
        try {
            $response = $this->client->sendRequest($this->getRequest($event));
            Log::info($response->getBody()->getContents());
        } catch (\Exception $exception) {
            Log::alert(FizzyLoggerHelper::makeContext() . FizzyLoggerHelper::getListenerLogPrefix(1) . $exception->getMessage());

            if ($this->attempts() < $this->tries) {
                $this->release(10);
            }
        }
    }

    private function getRequest(ClientRegisteredEvent $event): Request
    {
        return new Request('POST', $this->uri, ['Content-Type' => 'application/json'], $event->userData->jsonSerialize());
    }
}

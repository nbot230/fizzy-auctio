<?php

use Illuminate\Support\Str;

if (! function_exists('normalizeEmail')) {
    /**
     * Normalize email.
     *
     * @param ?string $email
     *
     * @return string
     */
    function normalizeEmail(?string $email): string
    {
        return Str::lower(\trim((string) $email));
    }
}

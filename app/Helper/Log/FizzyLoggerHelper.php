<?php

namespace App\Helper\Log;

use Illuminate\Support\Str;

class FizzyLoggerHelper
{
    // результат выполнения операции
    const PASSED = 'PASSED'; // успех
    const FAILED = 'FAILED'; // ошибка

    //логика работы
    public const AUTH = 'auth';
    public const CONTROLLER = 'controller';
    public const REPOSITORY = 'repository';
    public const COMMAND = 'command';
    public const OBJECT_INTEREST_SERVICE = 'object_interest_service';
    public const CHAT_DATA_HELPER_SERVICE = 'chat_data_helper_service';
    public const SMS_SERVICE = 'sms_service';
    public const QUEUE_CONSUMER = 'queue_consumer';
    public const REQUEST_NORMALIZER = 'request_normalizer';
    public const LISTENER = 'listener_queue';

    /**
     * @param int $step
     *
     * @return string
     */
    public static function getAuthLogPrefix(int $step): string
    {
        return self::AUTH . ":{$step} step: ";
    }

    public static function getListenerLogPrefix(): string
    {
        return self::LISTENER;
    }

    /**
     * @param int $step
     *
     * @return string
     */
    public static function getControllerLogPrefix(int $step): string
    {
        return self::CONTROLLER . ":{$step} step: ";
    }

    /**
     * @param int $step
     *
     * @return string
     */
    public static function getRepositoryLogPrefix(int $step): string
    {
        return self::REPOSITORY . ":{$step} step: ";
    }

    /**
     * @param int $step
     *
     * @return string
     */
    public static function getCommandLogPrefix(int $step): string
    {
        return self::COMMAND . ":{$step} step: ";
    }

    /**
     * @return string
     */
    public static function makeContext(): string
    {
        return "[" . Str::random(6) . "] ";
    }
}

<?php

namespace App\FormRequest\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginFormRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => 'required|max:255',
            'password' => 'required|min:6|max:255',
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->get('email');
    }

    /**
     * @return string|null
     */
    public function getPass(): ?string
    {
        return $this->get('password');
    }
}

<?php

namespace App\Repository;


use App\DTO\Criteria\CriteriaDTO;

abstract class BaseRepository
{
    /**
     * @param CriteriaDTO $criteriaDTO
     *
     * @return string
     *
     * @throws \JsonException
     */
    protected function prepareCriteriaForLog(CriteriaDTO $criteriaDTO): string
    {
        $criteria = [];

        foreach ($criteriaDTO->getCriteria() as $criterionName => $criterionValue) {
            $criterionValue = is_array($criterionValue) ? json_encode($criterionValue, JSON_THROW_ON_ERROR) : $criterionValue;

            $criteria[] = $criterionName . ' - ' . $criterionValue;
        }

        return implode(';', $criteria);
    }
}

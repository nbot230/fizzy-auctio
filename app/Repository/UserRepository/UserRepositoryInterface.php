<?php

namespace App\Repository\UserRepository;

use App\DTO\Criteria\CriteriaDTO;
use App\DTO\User\UserDTO;
use App\Models\User;

interface UserRepositoryInterface
{
    public function retrieveById(string $id): User;

    public function retrieveByToken(string $token);

    public function create(UserDTO $userDTO): User;

    public function findByEmail(CriteriaDTO $userDTO): User;

    public function update(UserDTO $userDTO): void;

    public function delete($id): void;
}

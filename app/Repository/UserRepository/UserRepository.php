<?php

namespace App\Repository\UserRepository;

use App\DTO\Criteria\CriteriaDTO;
use App\DTO\Criteria\UserCriteria\UserCriteriaDTO;
use App\DTO\Role\Role;
use App\DTO\User\UserDTO;
use App\Helper\Log\FizzyLoggerHelper;
use App\Repository\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use App\Models\User;
use App\Repository\Exception\UnprocessibleEntityException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function create(UserDTO $userDTO): User
    {

        $userData = $userDTO->jsonSerialize();

        try {
            $client = new User($userData);
            $client->password = $userData['password'];
            $client->email = User::normalizeEmail($userData['email']);

            $client->save();
        } catch (\Throwable $exception) {
            Log::critical(
                FizzyLoggerHelper::makeContext(). FizzyLoggerHelper::getRepositoryLogPrefix(1) ."Saving {$this->getModelName()} failed"
            );

            throw new UnprocessibleEntityException($exception);
        }

        $client->assignRole(Role::UNVERIFIED);

        return $client;
    }

    /**
     * @param $id
     * @return User
     */
    public function retrieveById($id): User
    {
        try {
            $user = User::find($id)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            Log::alert('Поиск пользователя по ID. -' .
                FizzyLoggerHelper::FAILED . ", " .
                "Составление квери и запрос на поиск по ID {$id})");

            throw new ModelNotFoundException('Операция по поиску пользователя по ID' . FizzyLoggerHelper::FAILED);
        }

        return $user;
    }

    public function retrieveByToken(string $token)
    {
        return new User();
    }

    /**
     * @param CriteriaDTO $criteriaDTO
     *
     * @return User
     *
     * @throws \JsonException
     */
    public function findByEmail(CriteriaDTO $criteriaDTO): User
    {
        Log::info(
            FizzyLoggerHelper::getRepositoryLogPrefix(1) .
            'Поиск пользователя по email. -' .
            FizzyLoggerHelper::PASSED . ", " .
            "Составление квери и запрос на поиск по имейлу {$criteriaDTO->getCriteria(UserCriteriaDTO::USER_EMAIL)} со следующими данными - "
            . $this->prepareCriteriaForLog($criteriaDTO)
        );

        try {
            /** @var User $user */
            $user = $this
                ->getUserQuery()
                ->where('email', '=', $criteriaDTO->getCriteria(UserCriteriaDTO::USER_EMAIL))
                ->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            Log::info("Не удалось найти покупателя по критериям " . $this->prepareCriteriaForLog($criteriaDTO));

            throw new ModelNotFoundException("Не удалось найти покупателя по критериям " . $this->prepareCriteriaForLog($criteriaDTO));
        }

        return $user;
    }

    public function update(UserDTO $userDTO): void
    {
        Log::info(
            FizzyLoggerHelper::getRepositoryLogPrefix(1) .
            'Поиск пользователя по email. -' .
            FizzyLoggerHelper::PASSED . ", " .
            "Составление квери и запрос на поиск по имейлу {$userDTO->email}"
        );
    }

    public function delete($id): void
    {
        // TODO: Implement delete() method.
    }

    /**
     * @return Builder
     */
    private function getUserQuery(): Builder
    {
        return User::query()->withoutGlobalScopes();
    }

    protected function getModelName(): string
    {
        return User::class;
    }
}

<?php

namespace App\Repository;

abstract class BaseCriteria
{
    public const ALL_LIMIT = 'all_limit';

    public const DEFAULT_ORDER_DIRECTION = self::DESC;

    public const ASC = 'ASC';
    public const DESC = 'DESC';
}

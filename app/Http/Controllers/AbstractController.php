<?php

namespace App\Http\Controllers;

use App\Helper\Log\FizzyLoggerHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AbstractController extends Controller
{
    /**
     * @param Request $request
     * @param string $message
     * @param int $logStep
     *
     * @throws \JsonException
     */
    protected function logStartAction(Request $request, string $message, int $logStep = 1): void
    {
        $referer = $request->headers->get('referer');
        Log::info(
            FizzyLoggerHelper::getControllerLogPrefix($logStep) .
            $message . ". - " . FizzyLoggerHelper::PASSED .
            ", Запуск обработки запроса от " . ($referer ?: "неопределенного урла") . " с параметрами: '" .
            json_encode($request->query->all(), JSON_THROW_ON_ERROR) . "'"
        );
    }

    /**
     * @param string $action
     * @param int $step
     * @param string $message
     * @param string|null $data
     */
    protected function logSuccessfulControllerAction(
        string $action,
        int $step,
        string $message,
        string $data = null
    ): void {
        $wholeMessage =
            FizzyLoggerHelper::getControllerLogPrefix($step).
            $action . '. - ' .
            FizzyLoggerHelper::PASSED ." - ". $message;
        Log::info(
            $wholeMessage . \is_null($data) . ' ' . $data
        );
    }

    protected function logControllerFail(
        string $action,
        int $step,
        string $message,
        string $data = null
    ): void {
        $wholeMessage =
            FizzyLoggerHelper::getControllerLogPrefix($step).
            $action . '. - ' .
            FizzyLoggerHelper::FAILED ." - ". $message;
        Log::info(
            $wholeMessage . \is_null($data) . ' ' . $data
        );
    }

    protected function extractRequestData(Request $request): array
    {
        return $this->prepareArray($request->toArray());
    }

    private function prepareArray(array $array): array
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $array[$key] = $this->prepareArray($value);
            } elseif ($value === "") {
                $array[$key] = null;
            }
        }

        return $array;
    }
}

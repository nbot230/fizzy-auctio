<?php

namespace App\Http\Controllers;

use App\DTO\Criteria\CriteriaDTO;
use App\DTO\Criteria\UserCriteria\UserCriteriaDTO;
use App\DTO\Response\AuthResponse\UserAuthResponseDTO;
use App\DTO\Role\Role;
use App\DTO\User\UserDTO;
use App\Event\ClientRegisteredEvent;
use App\FormRequest\Auth\LoginFormRequest;
use App\Models\User;
use App\Repository\UserRepository\UserRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends AbstractController
{
    /**
     * @OA\Info(
     *      version="1.0.0",title="FIZZY AUCTION", description="AUCTIO IS A BRAND NEW AUCTION FOR SELLING VINTAGE CLOTHES",
     *      @OA\Contact(
     *          email="nbot230@gmail.com"
     *      )
     * ),
     *
     * @OA\Server(url=L5_SWAGGER_CONST_HOST,description="Fizzy server"),
     *
     * @OA\Tag(name="FIZZY",description="API Endpoints of Project"),
     */
    private UserRepositoryInterface $userRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @OA\Post(
     *     description="Метод для логина юзера",
     *     summary="Получение токена пользователя для авторизации запросов",
     *     tags={"Юзеры"},
     *     path="/auth/sign-in",
     *     @OA\Parameter(name="email", description="Email пользователя", required=true, in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(name="password", description="Password пользователя",required=true,in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *      @OA\Response(response=200, description="Результат логина пользователя",
     *          @OA\JsonContent(type="object", ref="#/components/schemas/UserAuthResponseDTO")
     *     ),
     *     @OA\Response(response="500", description="Ошибка сервера")
     * )
     * @param LoginFormRequest $request
     * @return JsonResponse
     *
     * @throws \JsonException
     */
    public function loginAction(
        LoginFormRequest $request
    ): JsonResponse
    {
        $message = 'Логин пользователя в системе';
        $this->logStartAction($request, $message);

        $criteria = (new CriteriaDTO())->setCriteria([
            UserCriteriaDTO::USER_EMAIL => $request->getEmail()
        ]);

        try {
            $user = $this->userRepository->findByEmail($criteria);
        }  catch (ModelNotFoundException $exception) {

            return new JsonResponse(new UserAuthResponseDTO([
                'success' => false,
                'message' => $exception->getMessage()
            ]), Response::HTTP_BAD_REQUEST);
        }

        /* check pass action */
        if (Hash::check($request->getPass(), $user->password)) {

            return new JsonResponse((new UserAuthResponseDTO([
                'success' => true,
                'token' => $user->createToken('user_token', $this->getUserAbilities($user))->plainTextToken,
            ]))->jsonSerialize(), Response::HTTP_OK);
        }

        return new JsonResponse(['success' => false], Response::HTTP_FORBIDDEN);
    }

    /**
     * @OA\Post(
     *     path="/auth/sign-up",
     *     description="Метод для регистрации пользователя",
     *     tags={"Юзеры"},
     *      @OA\Parameter(name="email", description="Email пользователя", required=true, in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="phone", description="Phone пользователя", required=true, in="path",
     *          @OA\Schema(type="string",example="+7999999999")
     *     ),
     *     @OA\Parameter(
     *          name="password", description="Password пользователя",required=true, in="path",
     *          @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response=200, description="Результат регистрации",
     *          @OA\JsonContent(type="object", ref="#/components/schemas/UserAuthResponseDTO")
     *     ),
     *     @OA\Response(response="500", description="Ошибка Создания юзера", ref="#/components/schemas/UserAuthResponseDTO"),
     *     @OA\Response(response="422", description="Ошибка Создания юзера", ref="#/components/schemas/UserAuthResponseDTO"),
     * )
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \JsonException
     */
    public function createAction(
        Request $request,
    ): JsonResponse {
        $message = 'Регистрация пользователя в системе';
        $this->logStartAction($request, $message);

        $clientData = $this->extractRequestData($request);

        $validator = $this->validateDataForCreatingUser($clientData);

        if ($this->validatorFails($validator, $clientData)) {

            return new JsonResponse(new UserAuthResponseDTO([
                'success' => false,
                'errors' => $validator->errors()->toArray(),
                'message' => 'Cannot validate user data'
            ]), Response::HTTP_BAD_REQUEST);
        }

        $clientData['password'] = Hash::make(($clientData['password'] ?? Str::random(10)));

        $user = $this->userRepository->create($userDTO = new UserDTO($clientData));

        $token = $user->createToken('user_token', Role::ROLES_MAP[Role::UNVERIFIED])->plainTextToken;

        ClientRegisteredEvent::dispatch($userDTO);

        return new JsonResponse((new UserAuthResponseDTO([
            'success' => true,
            'token' => $token
        ]))->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();

        if ($user) {
            $user->tokens()->delete();
            return new JsonResponse(new UserAuthResponseDTO(['success' => true]), 201);
        }

        return new JsonResponse(new UserAuthResponseDTO(['success' => false]), Response::HTTP_FORBIDDEN);
    }

    /**
     * @return array<string>
     */
    private function getUserAbilities(User $user): array
    {
        $abilities = \array_map(function ($role) {
            return \implode(',', Role::ROLES_MAP[$role]);
        }, $user->getRoleNames()->toArray());

        return [implode(',', $abilities)];
    }

    /**
     * @param Validator $validator
     * @param $clientData
     *
     * @return bool
     */
    protected function validatorFails(Validator $validator, $clientData): bool
    {
        if ($validator->fails()) {
            $this->logControllerFail(
                'Валидация запроса',
                2,
                $validator->getMessageBag()->toJson(),
                \json_encode($clientData)
            );
        }

        return $validator->fails();
    }

    /**
     * @param array $data
     *
     * @return Validator
     */
    private function validateDataForCreatingUser(array $data): Validator
    {
        return ValidatorFacade::make($data, [
            'firstName' => 'required|min:',
            'email' => 'required|max:255|unique:users',
            'phone'    => 'required|max:255',
            'password' => 'required|min:6',
        ]);
    }
}

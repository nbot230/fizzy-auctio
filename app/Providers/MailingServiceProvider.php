<?php

namespace App\Providers;

use Illuminate\Config\Repository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Config\Repository as ConfigRepository;

class MailingServiceProvider extends ServiceProvider
{
    private const PROVIDER = 'mailopost';

    public function register()
    {
        $pathToProvider = 'mail.credentials.providers';
        /** @var Repository $config */
        $config  = $this->app->make(ConfigRepository::class);

        $apiKey = $config->get($pathToProvider . self::PROVIDER . 'key');
        $uri = $config->get($pathToProvider . self::PROVIDER . 'url');
        $password = $config->get($pathToProvider . self::PROVIDER . 'password');

    }
}

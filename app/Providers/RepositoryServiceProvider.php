<?php

namespace App\Providers;

use App\Repository\UserRepository\UserRepository;
use App\Repository\UserRepository\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerBindings();
    }

    protected function registerBindings(): void
    {
        foreach ($this->getBindingsMap() as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }

    protected function getBindingsMap(): array
    {
        return [
            UserRepositoryInterface::class => UserRepository::class
        ];
    }
}

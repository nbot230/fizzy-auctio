<?php

namespace App\Providers;

use App\Event\ClientRegisteredEvent;
use App\Listener\ClientRegisteredListener;
use GuzzleHttp\Client;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Config\Repository;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    private const PROVIDER = 'mailopost';
    private const AVAILABLE_TRIES_TO_RELEASE_JOB = 2;

    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ClientRegisteredEvent::class => [
            ClientRegisteredListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind(ClientRegisteredListener::class, function (Application $app) {
            $pathToProvider = 'mail.credentials.providers';

            /** @var Repository $config */
            $config  = $this->app->make(ConfigRepository::class);

            $apiKey = $config->get($pathToProvider . self::PROVIDER . 'key');
            $uri = $config->get($pathToProvider . self::PROVIDER . 'url');
            $password = $config->get($pathToProvider . self::PROVIDER . 'password');
            $tries = self::AVAILABLE_TRIES_TO_RELEASE_JOB;

            return new ClientRegisteredListener(new Client([
                'base_uri' => $uri
            ]), $tries, $apiKey);
        });
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}

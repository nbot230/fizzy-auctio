<?php

namespace App\DTO\Role;

class Role
{
    public const
        ADMIN = 'admin_super',
        USER = 'user',
        UNVERIFIED = 'unverified',
        MODERATOR = 'moderator';

    public const
            READ_USERS = 'read_users',
            DELETE_USERS = 'delete_users',
            UPDATE_USER = 'update_user',
            ADD_PRODUCT = 'add_product',
            UPDATE_PRODUCT = 'update_product',
            DELETE_PRODUCT = 'delete_product',
            ADD_COLOR = 'add_color',
            UPDATE_COLOR = 'update_color',
            DELETE_COLOR = 'delete_color',
            ADD_MATERIAL = 'add_material',
            UPDATE_MATERIAL = 'update_material',
            DELETE_MATERIAL = 'delete_material',
            ADD_TAG = 'add_tag',
            UPDATE_TAG = 'update_tag',
            DELETE_TAG = 'delete_tag',
            ADD_BRAND = 'add_brand',
            UPDATE_BRAND = 'update_brand',
            DELETE_BRAND = 'delete_brand';


    public const ROLES_MAP = [
        self::ADMIN => [self::READ_USERS, self::DELETE_USERS, self::UPDATE_USER],
        self::UNVERIFIED => [self::ADD_TAG, self::ADD_PRODUCT]
    ];
}

<?php

namespace App\DTO\Response\AuthResponse;

/**
 * Class UserAuthResponseDTO
 *
 * @package App\DTO\Response\AuthResponse;
 *
 * @OA\Schema(
 *     title="Ответ при аутентификации пользователя",
 *     type="object",
 *   )
 */
class UserAuthResponseDTO extends \App\DTO\BaseDTO implements \JsonSerializable
{
    /**
     * @OA\Property(
     *     property="success",
     *     description="Успешность запроса",
     *     type="bool | null",
     *     example="false"
     * )
     *
     * @var bool
     */
    public ?bool $success;


    /**
     * @OA\Property(
     *     property="token",
     *     description="Токен в случае успешной авторизации",
     *     type="string | null",
     *     example="FKqnw12FQ124qwgq1"
     * )
     *
     * @var string|null
     */
    public ?string $token;

    /**
     * @OA\Property(
     *     property="errors",
     *     description="Ошибки",
     *     @OA\Items(type="string")
     * )
     *
     * @var array
     */
    public array $errors;

    /**
     * @OA\Property(
     *     property="message",
     *     description="Сообщение в случае ошибки",
     *     type="string | null",
     *     example="Не удалось создать пользователя"
     * )
     *
     * @var string|null
     */
    public ?string $message;

    public function __construct(array $data)
    {
        $this->success = $data['success'] ?? false;
        $this->token = $data['token'] ?? null;
        $this->errors = $data['errors'] ?? [];
        $this->message = $data['message'] ?? null;
    }

    public function jsonSerialize()
    {
        return [
            'success' => $this->success,
            'token' => $this->token
        ];
    }
}

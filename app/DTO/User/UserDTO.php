<?php

namespace App\DTO\User;

use App\DTO\BaseDTO;
use App\DTO\Role\Role;

class UserDTO extends BaseDTO implements \JsonSerializable
{
    public ?string $id;
    /**
     * @var string|null
     */
    public ?string $firstName;

    public ?string $lastName;

    public ?string $email;

    public ?string $password;

    public ?string $phone;

    /**
     * @var array<Role>
     */
    public array $roles;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->id = $data['id'] ?? null;
        $this->firstName = $data['firstName'] ?? null;
        $this->lastName =  $data['lastName'] ?? null;
        $this->email = $data['email'] ?? null;
        $this->password = $data['password'] ?? null;
        $this->phone = $data['phone'] ?? null;
    }

    /**
     * @return array<string, string>
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'phone' => $this->phone,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'password' => $this->password,
        ];
    }

    /**
     * @param string[] $additional_params
     *
     * @return array<string|null>
     */
    public function withAdditionalParams(array $additional_params): array
    {
        return \array_merge($this->jsonSerialize(), $additional_params);
    }
}

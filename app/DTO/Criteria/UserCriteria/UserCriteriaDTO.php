<?php

namespace App\DTO\Criteria\UserCriteria;

class UserCriteriaDTO extends \App\Repository\BaseCriteria
{
    public const USER_ID = 'id';
    public const USER_EMAIL = 'email';
    public const USER_PHONE = 'phone';
}

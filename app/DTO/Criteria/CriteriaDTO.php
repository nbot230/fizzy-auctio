<?php

namespace App\DTO\Criteria;

/**
 * @package App\DTO\Repository
*/
class CriteriaDTO
{
    private array $criteria;

    public function __toString()
    {
        $str = "";
        foreach ($this->criteria as $name => $value) {
            $str .= "{$name}: {$value} ";
        }
        return $str;
    }

    /**
     * @param array $criteria
     */
    public function __construct(array $criteria = [])
    {
        $this->criteria = $criteria;
    }

    /**
     * @param array $criteria
     *
     * @return CriteriaDTO
     */
    public function setCriteria(array $criteria): CriteriaDTO
    {
        $this->criteria = $criteria;

        return $this;
    }

    /**
     * @param string $name
     * @param        $value
     *
     * @return CriteriaDTO
     */
    public function addCriterion(string $name, $value): CriteriaDTO
    {
        $this->criteria[$name] = $value;

        return $this;
    }

    /**
     * @param string|null $name
     * @param null        $defaultValue
     *
     * @return array|mixed|null
     */
    public function getCriteria(?string $name = null, $defaultValue = null)
    {
        if ($name) {
            return $this->criteria[$name] ?? $defaultValue;
        }

        return $this->criteria;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasCriterion(string $name): bool
    {
        return isset($this->criteria[$name]) && $this->criteria[$name];
    }
}

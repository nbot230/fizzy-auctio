<?php

namespace App\DTO;

class BaseDTO
{
    /**
     * @param \DateTime $dateTime
     *
     * @return string
     */
    public function prepareDateFormat(\DateTime $dateTime): string
    {
        return $dateTime->format(\DateTime::ATOM);
    }
}
